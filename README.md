# vue-projectManage
基于Vue.js实现的项目管理系统

需要配合[后端接口](https://gitee.com/vilson/ApiForProjectManage)使用

有不明白的地方的可以加群：275264059，或者联系我，QQ：545522390
### 演示地址
> [https://project.vilson.xyz](https://project.vilson.xyz)
### 环境 ###
- Node.js
### 安装依赖 ###
```
npm install
```
### 编译 ###
- 测试环境
```
npm run dev
访问 http://127.0.0.1:8090
```
- 生产环境 
```
npm run build
编译后将dist目录发布到服务端
```
### 登陆 ###
账号：admin 密码：123456
### 界面截图
![1](https://static.vilson.xyz/1.png)
![1](https://static.vilson.xyz/2.png)
![1](https://static.vilson.xyz/3.png)
![1](https://static.vilson.xyz/4.png)
![1](https://static.vilson.xyz/5.png)
![1](https://static.vilson.xyz/6.png)
![1](https://static.vilson.xyz/7.png)
